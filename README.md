# UDP SSH Tunnel

Easy to use UDP connections over SSH tunnels. 

Forwards a UDP port to a TCP port on server, creates an SSH tunnel from client to server on TCP port, and forwards TCP to UDP on client.

Uses only `ssh` and `nc`.

## Installation

First download the sample configuration file `udptunnel.cfg` and edit it to your liking.
	
	`curl -fsSL -O https://gitlab.com/j_ct/udp-ssh-tunnel/-/raw/master/udptunnel.cfg`

	`vi udptunnel.cfg`

Next you'll need to switch to root:

	`sudo su -`

Load the values from the configuration file to the environment:
	
	`set -a`

	`. ./udptunnel.cfg`

And finally run the install script, using either `client` or `server` as the subcommand, depending on which you are installing on.

You can download it yourself and run:

	`bash ./install.sh [client|server]

Or use this one liner:

	`curl -fsSL https://gitlab.com/j_ct/udp-ssh-tunnel/-/raw/master/install.sh | bash -s -- [client|server]`

After installation the environment variables can be edited directly in these files:

`/etc/default/udptunnel-client-forward@service-name`
`/etc/default/udptunnel-client-tunnel@service-name`
`/etc/default/udptunnel-server-forward@service-name`

You can interact with the services by running:

`sudo systemctl [status|stop|start] udptunnel-client-tunnel@service-name.service`
`sudo systemctl [status|stop|start] udptunnel-client-forward@service-name.service`
`sudo systemctl [status|stop|start] udptunnel-server-forward@service-name.service`

## Based On

http://zarb.org/~gc/html/udp-in-ssh-tunneling.html

https://wiki.networksecuritytoolkit.org/nstwiki/index.php/Tunnelling_UDP_Traffic_Through_An_SSH_Connection
