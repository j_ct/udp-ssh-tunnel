#!/usr/bin/env bash

git_base_url=${UDPTUN_GIT_BASE_URL:-https://gitlab.com/j_ct/udp-ssh-tunnel/-/raw/master}
bin_install_dir=${UDPTUN_BIN_INSTALL_DIR:-/usr/bin}

UDPTUN_FIFO_PATH=${UDPTUN_FIFO_PATH:-/tmp/fifo}

env_vars=(
	UDPTUN_SERVICE_NAME
    UDPTUN_SERVER_TCP_PORT
	UDPTUN_CLIENT_TCP_PORT
	UDPTUN_SERVER_UDP_PORT
	UDPTUN_CLIENT_UDP_PORT
	UDPTUN_SERVER_SSH_TARGET
	UDPTUN_SERVER_SSH_PORT
	UDPTUN_SERVER_SSH_ARGS
	UDPTUN_FIFO_PATH
)

fmt_error() {
  echo ${RED}"Error: $@"${RESET} >&2
}

setup_color() {
	# Only use colors if connected to a terminal
	if [ -t 1 ]; then
		RED=$(printf '\033[31m')
		GREEN=$(printf '\033[32m')
		YELLOW=$(printf '\033[33m')
		BLUE=$(printf '\033[34m')
		BOLD=$(printf '\033[1m')
		RESET=$(printf '\033[m')
	else
		RED=""
		GREEN=""
		YELLOW=""
		BLUE=""
		BOLD=""
		RESET=""
	fi
}

check_sudo(){
	if [ "$EUID" -ne 0 ]; then 
		fmt_error "Please run as root"
  		exit
	fi
}

check_env_vars(){
	for variable in "${env_vars[@]}"
	do
	    if [[ -z ${!variable+x} ]]; then
	        echo "${YELLOW}Warning: The environment variable \"${variable}\" should be defined.${RESET}"
	    fi
	done
}

write_env_file(){
	echo "" > "$1"
	for variable in "${env_vars[@]}"
	do
	    if [[ ! -z ${!variable+x} ]]; then
	        echo "$variable=${!variable}" >> "$1"
	    fi
	done
}

sub_help(){
    echo "Usage: $ProgName <subcommand> [options]\n"
    echo "Subcommands:"
    echo "    client    Install on client"
    echo "    server    Install on server"
    echo ""
    echo ""
}

install_binary(){
	curl -fsSL "${git_base_url}/bin/udptunnel" > "${bin_install_dir}/udptunnel"
	chmod +x "${bin_install_dir}/udptunnel"
}

install_service(){
	curl -fsSL "${git_base_url}/systemd/udptunnel-$1@.service" > "/etc/systemd/system/udptunnel-$1@.service"
	write_env_file "/etc/default/udptunnel-$1@$UDPTUN_SERVICE_NAME"
	sudo systemctl enable "udptunnel-$1@$UDPTUN_SERVICE_NAME"
}

start_service(){
	sudo systemctl stop "udptunnel-$1@$UDPTUN_SERVICE_NAME"
	sudo systemctl start "udptunnel-$1@$UDPTUN_SERVICE_NAME"
}

sub_client(){
	check_sudo
	check_env_vars
	install_binary
	install_service "client-tunnel"
	install_service "client-forward"
	systemctl daemon-reload
	start_service "client-tunnel"
	start_service "client-forward"
}

sub_server(){
	check_sudo
	check_env_vars
	install_binary
	install_service "server-forward"
	systemctl daemon-reload
	start_service "server-forward"
}

setup_color
	
echo "${BOLD}!!! UDP SSH Tunnel Installer !!!${RESET}"

ProgName=$(basename $0)

subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift
        sub_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac